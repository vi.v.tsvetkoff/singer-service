package ru.isys.trainings.singer.spring.annotationconfig.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.isys.trainings.singer.service.model.Album;
import ru.isys.trainings.singer.service.model.Genre;
import ru.isys.trainings.singer.service.model.Singer;
import ru.isys.trainings.singer.spring.annotationconfig.annotations.Logging;
import ru.isys.trainings.singer.spring.annotationconfig.dao.SingerDao;

import java.util.ArrayList;
import java.util.List;

@Service
public class SingerService {

//    @Autowired
//    @Qualifier("singerFileDao")
//    private SingerDao singerDao;

    private final SingerDao singerDao;

    public SingerService(@Qualifier("singerFileDao") SingerDao singerDao) {
        this.singerDao = singerDao;
    }

    @Logging
    public void findAndFilterByGenre(Genre genre) {
        List<Singer> singers = singerDao.findSingers();
        List<Singer> filteredSingers = filterByGenre(singers, genre);
        for (Singer singer : filteredSingers) {
            System.out.println(singer);
        }
        singerDao.saveSingers(filteredSingers);
    }

    private List<Singer> filterByGenre(List<Singer> singers, Genre genre) {
        List<Singer> result = new ArrayList<>();
        for (Singer singer : singers) {
            for (Album album : singer.getAlbums()) {
                if (album.getGenre() == genre) {
                    result.add(singer);
                    break;
                }
            }
        }
        return result;
    }

    private List<Singer> filterByMinAlbumsCount(List<Singer> singers, int minAlbums) {
        List<Singer> result = new ArrayList<>();
        for (Singer singer : singers) {
            if (singer.getAlbums().size() >= minAlbums) {
                result.add(singer);
            }
        }
        return result;
    }

}
