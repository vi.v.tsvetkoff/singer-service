package ru.isys.trainings.singer.spring.javaconfig;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.isys.trainings.singer.service.model.Genre;
import ru.isys.trainings.singer.spring.javaconfig.service.SingerService;

@Configuration
@ComponentScan
public class Application {

    public static void main(String[] args) {
        ApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(Application.class);

        SingerService singerService = applicationContext.getBean(SingerService.class);
        singerService.findAndFilterByGenre(Genre.GENRE1);
    }

}
