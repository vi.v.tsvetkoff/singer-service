package ru.isys.trainings.singer.spring.javaconfig.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import ru.isys.trainings.singer.service.dao.SingerDao;
import ru.isys.trainings.singer.service.dao.SingerFileDao;
import ru.isys.trainings.singer.service.dao.SingerJdbcDao;
import ru.isys.trainings.singer.spring.javaconfig.service.SingerService;

@Configuration
// @Import(PropertyConfiguration.class)
public class SingerConfiguration {

    @Bean
    @Primary
    public SingerDao singerFileDao(@Value("${data.singer.dao.file.input}") String inputFile,
                                   @Value("${data.singer.dao.file.output}") String outputFile) {
        return new SingerFileDao(inputFile, outputFile);
    }

    @Bean
    public SingerDao singerJdbcDao(@Value("${data.singer.dao.jdbc.url}") String jdbcUrl,
                                   @Value("${data.singer.dao.jdbc.username}") String username,
                                   @Value("${data.singer.dao.jdbc.password}") String password) {
        return new SingerJdbcDao(jdbcUrl, username, password);
    }

    @Bean
    public SingerService singerService(@Qualifier("singerFileDao") SingerDao singerDao) {
        return new SingerService(singerDao);
    }

}
