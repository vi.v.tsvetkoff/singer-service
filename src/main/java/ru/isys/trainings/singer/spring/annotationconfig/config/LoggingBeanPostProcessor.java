package ru.isys.trainings.singer.spring.annotationconfig.config;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import ru.isys.trainings.singer.spring.annotationconfig.annotations.Logging;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Component
public class LoggingBeanPostProcessor implements BeanPostProcessor {

    private final Set<String> requiredBeans = new HashSet<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = bean.getClass();
        for (Method method : beanClass.getMethods()) {
            if (method.isAnnotationPresent(Logging.class)) {
                requiredBeans.add(beanName);
                break;
            }
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (!requiredBeans.contains(beanName)) {
            // no need proxy
            return bean;
        }

        ProxyFactory proxyFactory = new ProxyFactory(bean);
        proxyFactory.addAdvice(new MethodInterceptor() {
            @Override
            public Object invoke(MethodInvocation invocation) throws Throwable {
                if (!invocation.getMethod().isAnnotationPresent(Logging.class)) {
                    return invocation.proceed();
                }

                System.out.println("Call method " + beanName + "." + invocation.getMethod().getName() +
                        " with arg: " + Arrays.toString(invocation.getArguments()));
                Object returnValue = invocation.proceed();
                System.out.println("Return from method " + beanName + "." + invocation.getMethod().getName() +
                        " : " + returnValue);
                return returnValue;
            }
        });
        return proxyFactory.getProxy();
    }

}
