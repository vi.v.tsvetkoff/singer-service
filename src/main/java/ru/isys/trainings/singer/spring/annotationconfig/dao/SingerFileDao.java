package ru.isys.trainings.singer.spring.annotationconfig.dao;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import ru.isys.trainings.singer.service.model.Album;
import ru.isys.trainings.singer.service.model.Genre;
import ru.isys.trainings.singer.service.model.Singer;
import ru.isys.trainings.singer.service.model.Song;
import ru.isys.trainings.singer.spring.annotationconfig.annotations.Logging;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SingerFileDao implements SingerDao {

    private final String inputFile;
    private final String outputFile;

    public SingerFileDao(@Value("${data.singer.dao.file.input}") String inputFile,
                         @Value("${data.singer.dao.file.output}") String outputFile) {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
    }

    @Override
    @Logging
    public List<Singer> findSingers() {
        try {
            List<Singer> singers = new ArrayList<>();

            List<String> lines = Files.readAllLines(Path.of(inputFile), StandardCharsets.UTF_8);
            for (String line : lines) {
                if (line.startsWith("Singer: ")) {
                    String singerName = line.substring("Singer: ".length());

                    Singer singer = new Singer(singerName, new ArrayList<>());
                    singers.add(singer);
                } else if (line.startsWith("    Album: ")) {
                    String albumNameAndGenreStr = line.substring("    Album: ".length());
                    String[] albumNameAndGenre = albumNameAndGenreStr.split(", ");
                    String albumName = albumNameAndGenre[0];
                    Genre genre = Genre.valueOf(albumNameAndGenre[1]);

                    Album album = new Album(albumName, genre, new ArrayList<>());
                    Singer currentSinger = singers.get(singers.size() - 1);
                    currentSinger.getAlbums().add(album);
                } else if (line.startsWith("        Song: ")) {
                    String songNameAndDurationStr = line.substring("        Song: ".length());
                    String[] songNameAndDuration = songNameAndDurationStr.split(", ");
                    String songName = songNameAndDuration[0];
                    int duration = Integer.parseInt(songNameAndDuration[1]);

                    Song song = new Song(songName, duration);
                    Singer currentSinger = singers.get(singers.size() - 1);
                    Album currentAlbum = currentSinger.getAlbums().get(currentSinger.getAlbums().size() - 1);
                    currentAlbum.getSongs().add(song);
                }
            }
            return singers;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    @Logging
    public void saveSingers(List<Singer> singers) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))) {
            for (Singer singer : singers) {
                writer.write("Singer: " + singer.getName());
                writer.newLine();
                for (Album album : singer.getAlbums()) {
                    writer.write("    Album: " + album.getName() + ", " + album.getGenre());
                    writer.newLine();
                    for (Song song : album.getSongs()) {
                        writer.write("        Song: " + song.getName() + ", " + song.getDuration());
                        writer.newLine();
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
