package ru.isys.trainings.singer.spring.javaconfig.service;

import ru.isys.trainings.singer.service.dao.SingerDao;
import ru.isys.trainings.singer.service.model.Album;
import ru.isys.trainings.singer.service.model.Genre;
import ru.isys.trainings.singer.service.model.Singer;

import java.util.ArrayList;
import java.util.List;

public class SingerService {

    private final SingerDao singerDao;

    public SingerService(SingerDao singerDao) {
        this.singerDao = singerDao;
    }

    public void findAndFilterByGenre(Genre genre) {
        List<Singer> singers = singerDao.findSingers();
        List<Singer> filteredSingers = filterByGenre(singers, genre);
        for (Singer singer : filteredSingers) {
            System.out.println(singer);
        }
        singerDao.saveSingers(filteredSingers);
    }

    private List<Singer> filterByGenre(List<Singer> singers, Genre genre) {
        List<Singer> result = new ArrayList<>();
        for (Singer singer : singers) {
            for (Album album : singer.getAlbums()) {
                if (album.getGenre() == genre) {
                    result.add(singer);
                    break;
                }
            }
        }
        return result;
    }

    private List<Singer> filterByMinAlbumsCount(List<Singer> singers, int minAlbums) {
        List<Singer> result = new ArrayList<>();
        for (Singer singer : singers) {
            if (singer.getAlbums().size() >= minAlbums) {
                result.add(singer);
            }
        }
        return result;
    }

}
