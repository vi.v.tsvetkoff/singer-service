package ru.isys.trainings.singer.service.model;

import java.util.List;

public class Album {

    private final String name;
    private final Genre genre;
    private final List<Song> songs;

    public Album(String name, Genre genre, List<Song> songs) {
        this.name = name;
        this.genre = genre;
        this.songs = songs;
    }

    public String getName() {
        return name;
    }

    public Genre getGenre() {
        return genre;
    }

    public List<Song> getSongs() {
        return songs;
    }

    @Override
    public String toString() {
        return "Album{" +
                "name='" + name + '\'' +
                ", genre=" + genre +
                ", songs=" + songs +
                '}';
    }

}
