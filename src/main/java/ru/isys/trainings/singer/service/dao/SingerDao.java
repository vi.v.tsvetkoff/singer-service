package ru.isys.trainings.singer.service.dao;

import ru.isys.trainings.singer.service.model.Singer;

import java.util.List;

public interface SingerDao {

    List<Singer> findSingers();

    void saveSingers(List<Singer> singers);

}
