package ru.isys.trainings.singer.service.dao;

import ru.isys.trainings.singer.service.model.Album;
import ru.isys.trainings.singer.service.model.Genre;
import ru.isys.trainings.singer.service.model.Singer;
import ru.isys.trainings.singer.service.model.Song;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SingerJdbcDao implements SingerDao {

    private static final String SQL_FIND_SINGERS = "SELECT s.id AS singer_id,\n" +
            "       s.name AS singer_name,\n" +
            "       a.id AS album_id,\n" +
            "       a.name AS album_name,\n" +
            "       a.genre AS genre,\n" +
            "       s2.id AS song_id,\n" +
            "       s2.name AS song_name,\n" +
            "       s2.duration AS duration\n" +
            "FROM singers s\n" +
            "INNER JOIN albums a ON s.id = a.singer_id\n" +
            "INNER JOIN songs s2 on a.id = s2.album_id";

    private final String jdbcUrl;
    private final String userName;
    private final String password;

    public SingerJdbcDao(String jdbcUrl, String userName, String password) {
        this.jdbcUrl = jdbcUrl;
        this.userName = userName;
        this.password = password;
    }

    @Override
    public List<Singer> findSingers() {
        try (Connection connection = DriverManager.getConnection(jdbcUrl, userName, password);
             Statement statement = connection.createStatement()) {

            Map<Integer, Singer> singersMap = new HashMap<>();
            Map<Integer, Album> albumsMap = new HashMap<>();
            Map<Integer, Song> songsMap = new HashMap<>();

            ResultSet resultSet = statement.executeQuery(SQL_FIND_SINGERS);
            while (resultSet.next()) {
                int singerId = resultSet.getInt("singer_id");
                if (!singersMap.containsKey(singerId)) {
                    String singerName = resultSet.getString("singer_name");
                    Singer singer = new Singer(singerName, new ArrayList<>());
                    singersMap.put(singerId, singer);
                }

                int albumId = resultSet.getInt("album_id");
                if (!albumsMap.containsKey(albumId)) {
                    String albumName = resultSet.getString("album_name");
                    Genre genre = Genre.valueOf(resultSet.getString("genre"));

                    Album album = new Album(albumName, genre, new ArrayList<>());
                    albumsMap.put(albumId, album);

                    singersMap.get(singerId).getAlbums().add(album);
                }

                int songId = resultSet.getInt("song_id");
                if (!songsMap.containsKey(songId)) {
                    String songName = resultSet.getString("song_name");
                    int duration = resultSet.getInt("duration");

                    Song song = new Song(songName, duration);
                    songsMap.put(songId, song);

                    albumsMap.get(albumId).getSongs().add(song);
                }
            }

            return new ArrayList<>(singersMap.values());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void saveSingers(List<Singer> singers) {

    }

}
