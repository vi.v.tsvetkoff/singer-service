package ru.isys.trainings.singer.service.model;

import java.util.List;

public class Singer {

    private final String name;
    private final List<Album> albums;

    public Singer(String name, List<Album> albums) {
        this.name = name;
        this.albums = albums;
    }

    public String getName() {
        return name;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    @Override
    public String toString() {
        return "Singer{" +
                "name='" + name + '\'' +
                ", albums=" + albums +
                '}';
    }
}
