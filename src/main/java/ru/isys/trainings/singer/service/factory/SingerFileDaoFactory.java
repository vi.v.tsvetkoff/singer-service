package ru.isys.trainings.singer.service.factory;

import ru.isys.trainings.singer.service.dao.SingerDao;
import ru.isys.trainings.singer.service.dao.SingerFileDao;

public class SingerFileDaoFactory implements SingerDaoFactory {

    private static final SingerDaoFactory INSTANCE = new SingerFileDaoFactory();

    private SingerFileDaoFactory() {

    }

    public static SingerDaoFactory getInstance() {
        return INSTANCE;
    }

    @Override
    public SingerDao createSingerDao() {
        return new SingerFileDao(
                "/home/vagrant/Documents/singers.txt",
                "/home/vagrant/Documents/filtered_singers.txt"
        );
    }

}
