package ru.isys.trainings.singer.service;

import ru.isys.trainings.singer.service.dao.SingerDao;
import ru.isys.trainings.singer.service.factory.SingerDaoFactory;
import ru.isys.trainings.singer.service.factory.SingerFileDaoFactory;
import ru.isys.trainings.singer.service.factory.SingerJdbcDaoFactory;
import ru.isys.trainings.singer.service.model.Singer;
import ru.isys.trainings.singer.service.service.SingerService;

import java.util.List;

public class Application {

    public static void main(String[] args) {
        String dataType = "database"; // "file", "database", "json", "xml"
        SingerDaoFactory singerDaoFactory = createSingerDaoFactory(dataType);

        SingerDao singerDao = singerDaoFactory.createSingerDao();
        List<Singer> singers = singerDao.findSingers();

        SingerService singerService = new SingerService();
        // List<Singer> filteredSingers = singerService.filterByGenre(singers, Genre.GENRE1);
        List<Singer> filteredSingers = singerService.filterByMinAlbumsCount(singers, 2);

        for (Singer singer : filteredSingers) {
            System.out.println(singer);
        }

        //singerDao.saveSingers(filteredSingers);
    }

    private static SingerDaoFactory createSingerDaoFactory(String dataType) {
        switch (dataType) {
            case "file":
                return SingerFileDaoFactory.getInstance();
            case "database":
                return SingerJdbcDaoFactory.getInstance();
            default:
                throw new IllegalArgumentException("Type " + dataType + " is not supported");
        }
    }

}
