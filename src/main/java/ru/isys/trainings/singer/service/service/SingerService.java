package ru.isys.trainings.singer.service.service;

import ru.isys.trainings.singer.service.model.Album;
import ru.isys.trainings.singer.service.model.Genre;
import ru.isys.trainings.singer.service.model.Singer;

import java.util.ArrayList;
import java.util.List;

public class SingerService {

    public List<Singer> filterByGenre(List<Singer> singers, Genre genre) {
        List<Singer> result = new ArrayList<>();
        for (Singer singer : singers) {
            for (Album album : singer.getAlbums()) {
                if (album.getGenre() == genre) {
                    result.add(singer);
                    break;
                }
            }
        }
        return result;
    }

    public List<Singer> filterByMinAlbumsCount(List<Singer> singers, int minAlbums) {
        List<Singer> result = new ArrayList<>();
        for (Singer singer : singers) {
            if (singer.getAlbums().size() >= minAlbums) {
                result.add(singer);
            }
        }
        return result;
    }

}

