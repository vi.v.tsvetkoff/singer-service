package ru.isys.trainings.singer.service.factory;

import ru.isys.trainings.singer.service.dao.SingerDao;

public interface SingerDaoFactory {

    SingerDao createSingerDao();

}
