package ru.isys.trainings.singer.service.factory;

import ru.isys.trainings.singer.service.dao.SingerDao;
import ru.isys.trainings.singer.service.dao.SingerJdbcDao;

public class SingerJdbcDaoFactory implements SingerDaoFactory {

    private static final SingerDaoFactory INSTANCE = new SingerJdbcDaoFactory();

    private SingerJdbcDaoFactory() {

    }

    public static SingerDaoFactory getInstance() {
        return INSTANCE;
    }

    @Override
    public SingerDao createSingerDao() {
        return new SingerJdbcDao(
                "jdbc:postgresql://localhost:5432/test_db",
                "test_user",
                "test_pass"
        );
    }

}
