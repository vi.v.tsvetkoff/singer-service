create table singers
(
    id serial not null
        constraint singers_pk
            primary key,
    name varchar not null
);

create table albums
(
    id serial not null
        constraint albums_pk
            primary key,
    name varchar not null,
    genre varchar not null,
    singer_id int
        constraint singer___fk
            references singers
);

create table songs
(
    id serial not null
        constraint songs_pk
            primary key,
    name varchar not null,
    duration int not null,
    album_id int
        constraint genre___fk
            references albums
);

insert into singers (name) values ('Singer1');
insert into singers (name) values ('Singer2');
insert into singers (name) values ('Singer3');

insert into albums (name, genre, singer_id) values ('AlbumName1', 'GENRE1', 1);
insert into albums (name, genre, singer_id) values ('AlbumName2', 'GENRE2', 1);
insert into albums (name, genre, singer_id) values ('AlbumName3', 'GENRE1', 2);
insert into albums (name, genre, singer_id) values ('AlbumName4', 'GENRE1', 2);
insert into albums (name, genre, singer_id) values ('AlbumName5', 'GENRE3', 3);

insert into songs (name, duration, album_id) values ('SongName1', 100, 1);
insert into songs (name, duration, album_id) values ('SongName2', 200, 1);
insert into songs (name, duration, album_id) values ('SongName3', 300, 1);
insert into songs (name, duration, album_id) values ('SongName4', 400, 2);
insert into songs (name, duration, album_id) values ('SongName5', 500, 2);
insert into songs (name, duration, album_id) values ('SongName6', 600, 2);
insert into songs (name, duration, album_id) values ('SongName7', 700, 2);
insert into songs (name, duration, album_id) values ('SongName8', 100, 3);
insert into songs (name, duration, album_id) values ('SongName9', 200, 3);
insert into songs (name, duration, album_id) values ('SongName10', 300, 3);
insert into songs (name, duration, album_id) values ('SongName11', 400, 3);
insert into songs (name, duration, album_id) values ('SongName12', 500, 3);
insert into songs (name, duration, album_id) values ('SongName13', 600, 3);
insert into songs (name, duration, album_id) values ('SongName14', 300, 4);
insert into songs (name, duration, album_id) values ('SongName15', 400, 4);
insert into songs (name, duration, album_id) values ('SongName16', 500, 4);
insert into songs (name, duration, album_id) values ('SongName17', 600, 4);
insert into songs (name, duration, album_id) values ('SongName18', 600, 4);
insert into songs (name, duration, album_id) values ('SongName19', 500, 5);
insert into songs (name, duration, album_id) values ('SongName20', 600, 5);
insert into songs (name, duration, album_id) values ('SongName21', 600, 5);
